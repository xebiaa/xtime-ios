//
//  XtimeProxyApiTest.swift
//  xtime-ios
//
//  Created by Steven Mulder on 8/22/14.
//  Copyright (c) 2014 Xebia Nederland B.V. All rights reserved.
//

import XCTest


class XtimeProxyApiTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        let api = XTimeProxyApi()
        let request = GetWeekOverviewRequest(firstDayOfWeek: "2014-08-11")
        api.getWeekOverview(request, callback: {
            (response: GetWeekOverviewResponse) -> Void in
            // TODO
            }, error: nil)
        
        XCTAssert(false, "Pass")
    }
}
