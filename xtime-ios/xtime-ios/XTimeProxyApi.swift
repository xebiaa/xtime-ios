//
//  XTimeProxyApi.swift
//  xtime-ios
//
//  Created by Steven Mulder on 8/22/14.
//  Copyright (c) 2014 Xebia Nederland B.V. All rights reserved.
//

import Foundation

class XTimeProxyApi : API {
    
    func getWeekOverview(request: GetWeekOverviewRequest, callback: GetWeekOverviewResponse -> Void, error: NSErrorPointer)  {
        
        let url = NSURL(string: "http://localhost:9000/week?date=2014-08-11")
        var request = NSMutableURLRequest(URL: url)
        request.setValue("username=smulder; JSESSIONID=5678C9CB30F010E03353107E93E1268E; _hp2_id.2790962031=4600497982186508.0.0; s_fid=0001184F49D05B24-3B6EB99660721A7C; __utma=179778477.69251742.1379142638.1391176259.1393790031.13; __utmz=179778477.1379142638.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _ga=GA1.2.69251742.1379142638; hsfirstvisit=http%3A%2F%2Ftraining.xebia.com%2Fsummer-specials%2Fsummer-special-automated-acceptance-testing-with-fitnesse-and-appium||1401696949959; __hstc=232286976.12e4110469efdd95968c50621192e0cc.1401696949961.1401696949961.1401696949961.1; hubspotutk=12e4110469efdd95968c50621192e0cc", forHTTPHeaderField: "Cookie")
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) {(data, response, error) in
            println("response")
        }
        
        task.resume()
        
        callback( GetWeekOverviewResponse(lastTransferred: 0, monthlyDataApproved: false, projects: [], timesheetRows: [], username: "") )
    }
    
    func getWorkTypesForProject(request: GetWorkTypesForProjectRequest, callback: GetWorkTypesForProjectResponse -> Void, error: NSErrorPointer) {
        callback( GetWorkTypesForProjectResponse(workTypes: []) )
    }
}