//
//  ApiMessages.swift
//  xtime-ios
//
//  Created by Barend Garvelink on 22-8-14.
//  Copyright (c) 2014 Xebia Nederland B.V. All rights reserved.
//

import Foundation

public typealias YearMonthDay = String // "yyyy-MM-dd"
public typealias DayAsTimeStamp = Double // UNIX timestamp, *must* resolve to midnight

public struct Project {
    let id: Int64
    let description: String
}

public struct WorkType {
    let id: Int64
    let description: String
}

public struct TimesheetRow {
    struct TimeCell {
        let approved: Bool
        let entryDate: DayAsTimeStamp
        let hours: Float32
    }
    let project: Project
    let workType: WorkType
    let description: String
    let timecells: [TimeCell]
}

public struct GetWeekOverviewRequest {
    let firstDayOfWeek: YearMonthDay
}

public struct GetWeekOverviewResponse {
    let lastTransferred: DayAsTimeStamp
    let monthlyDataApproved: Bool
    let projects: [Project]
    let timesheetRows: [TimesheetRow]
    let username: String
}

public struct GetWorkTypesForProjectRequest {
    let projectId: Int64
    let firstDayOfWeek: YearMonthDay
}

public struct GetWorkTypesForProjectResponse {
    let workTypes: [WorkType]
}
