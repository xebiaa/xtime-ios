//
//  TableViewCell.swift
//  xtime-ios
//
//  Created by Jan on 22/08/14.
//  Copyright (c) 2014 Xebia Nederland B.V. All rights reserved.
//

import UIKit

class XTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var hourLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
