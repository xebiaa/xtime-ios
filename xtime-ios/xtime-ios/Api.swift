//
//  Api.swift
//  xtime-ios
//
//  Created by Barend Garvelink on 22-8-14.
//  Copyright (c) 2014 Xebia Nederland B.V. All rights reserved.
//

import Foundation


public protocol API {
    func getWeekOverview(request: GetWeekOverviewRequest, callback: GetWeekOverviewResponse -> Void, error: NSErrorPointer)
    func getWorkTypesForProject(request: GetWorkTypesForProjectRequest, callback: GetWorkTypesForProjectResponse -> Void, error: NSErrorPointer)
}

public class DummyApi : API {
    let project1 = Project(id: 1, description: "Demo Project 1")
    let project2 = Project(id: 2, description: "Demo Project 2")
    let project3 = Project(id: 3, description: "Demo Project 3")
    let project1worktype1 = WorkType(id: 11, description: "Useful work")
    let project1worktype2 = WorkType(id: 12, description: "Useless work")
    let project2worktype1 = WorkType(id: 21, description: "Project 2 work")
    let project3worktype1 = WorkType(id: 31, description: "Misc work")

    public func getWeekOverview(request: GetWeekOverviewRequest, callback: GetWeekOverviewResponse -> Void, error: NSErrorPointer) {
        let monday    : DayAsTimeStamp = 1407708000000
        let tuesday   : DayAsTimeStamp = monday + 86400
        let wednesday : DayAsTimeStamp = tuesday + 86400
        callback(GetWeekOverviewResponse(
            lastTransferred: wednesday,
            monthlyDataApproved: false,
            projects: [project1, project2, project3],
            timesheetRows: [
                TimesheetRow(
                    project: project1,
                    workType: project1worktype1,
                    description: "All kinds of useful stuff",
                    timecells: [
                        TimesheetRow.TimeCell(approved: false, entryDate: monday, hours: 5.5),
                        TimesheetRow.TimeCell(approved: false, entryDate: tuesday, hours: 8)
                    ]
                ),
                TimesheetRow(
                    project: project1,
                    workType: project1worktype2,
                    description: "Some less useful stuff",
                    timecells: [
                        TimesheetRow.TimeCell(approved: false, entryDate: monday, hours: 1.5),
                    ]
                ),
                TimesheetRow(
                    project: project2,
                    workType: project2worktype1,
                    description: "Project 2 work is best work",
                    timecells: [
                        TimesheetRow.TimeCell(approved: false, entryDate: monday, hours: 1),
                    ]
                ),
                TimesheetRow(
                    project: project3,
                    workType: project3worktype1,
                    description: "Miscellaneous stuff",
                    timecells: [
                        TimesheetRow.TimeCell(approved: false, entryDate: wednesday, hours: 8),
                    ]
                ),
            ],
            username: "Dummy Duck"
        ))
    }

    public func getWorkTypesForProject(request: GetWorkTypesForProjectRequest, callback: GetWorkTypesForProjectResponse -> Void, error: NSErrorPointer) {
        var worktypes : [WorkType]
        switch (request.projectId) {
        case 1:
            worktypes = [project1worktype1, project1worktype2]
        case 2:
            worktypes = [project2worktype1]
        case 3:
            worktypes = [project3worktype1]
        default:
            worktypes = []
        }
        callback(GetWorkTypesForProjectResponse(workTypes: worktypes))
    }
}