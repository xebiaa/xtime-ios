//
//  ViewController.swift
//  xtime-ios
//
//  Created by Barend Garvelink on 22-8-14.
//  Copyright (c) 2014 Xebia Nederland B.V. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let api = DummyApi()
    let request = GetWeekOverviewRequest(firstDayOfWeek: "2014-08-11")
    var response : [DayEntry] = []
//    = GetWeekOverviewResponse(
//        lastTransferred: 0,
//        monthlyDataApproved: false,
//        projects: [],
//        timesheetRows: [],
//        username: "")
//    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView!, numberOfRowsInSection section: Int) -> Int {
        return response.count
    }
    
    func loadData(){
        var blah: NSError?
        api.getWeekOverview(request, callback: dataLoaded, error: &blah)
    }

    func dataLoaded(aresponse: GetWeekOverviewResponse) {
        self.response = convertToDayEntry(aresponse)
    }

    func tableView(tableView: UITableView!, cellForRowAtIndexPath indexPath: NSIndexPath!) -> UITableViewCell! {
        let cell = tableView.dequeueReusableCellWithIdentifier("XTimeTableViewCell", forIndexPath: indexPath) as XTimeTableViewCell
        
        let item = response[indexPath.row]
        let date = NSDate(timeIntervalSince1970: item.day)
        let dateFormatter = NSDateFormatter()

        dateFormatter.dateFormat = "EEEE dd MMM"
        cell.dateLabel.text = dateFormatter.stringFromDate(date)
        cell.hourLabel.text = NSString(format: "%.2f", item.hours)
        
        return cell
    }
    
    func convertToDayEntry(response: GetWeekOverviewResponse) -> [DayEntry] {
        var dictionary = Dictionary<DayAsTimeStamp, Float>()
        for timeRow in response.timesheetRows{
            for hourRow in timeRow.timecells{
                var dat: Float = dictionary[hourRow.entryDate] ?? 0
                dictionary[hourRow.entryDate] = dat + hourRow.hours
            }
        }
        
        var result : [DayEntry] = [];
        for (key, value) in dictionary{
            result.append(DayEntry(day: key, hours: value))
        }
        
        return result
    }
}

public struct DayEntry {
    let day: DayAsTimeStamp
    let hours: Float
}
